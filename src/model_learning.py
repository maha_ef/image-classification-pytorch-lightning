from argparse import ArgumentParser
import pytorch_lightning as pl
from clearml import Task

import warnings
warnings.filterwarnings("ignore")

from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader
from torch.optim import SGD, Adam
from torch import nn
import torch

from torchvision.datasets import ImageFolder
import torchvision.models as models
from torchvision import transforms
from torchmetrics import Accuracy

# import matplotlib.pyplot as plt
import os

class ResNetClassifier(pl.LightningModule):
    resnets = {
        18: models.resnet18,
        34: models.resnet34,
        50: models.resnet50
    }

    optimizers = {
        'sgd': SGD,
        'adam': Adam
    }

    schedulers = {
        'plateau': ReduceLROnPlateau,
        'b': 'vvv'
    }

    def __init__(self, 
                 num_classes: int, 
                 train_path: str, 
                 val_path: str, 
                 test_path: str = None, 
                 resnet_version: int = 18,
                 lr: float = 1e-3, 
                 batch_size: int = 16, 
                 optimizer: str = 'adam',
                 scheduler: str = None, 
                 transfer: bool = True, 
                 blocks_to_tune: int = 1
                ):
        super().__init__()

        self.num_classes = num_classes
        self.train_path = train_path
        self.val_path = val_path
        self.test_path = test_path
        self.lr = lr
        self.batch_size = batch_size

        self.optimizer = self.optimizers[optimizer]
        if scheduler is None:
            self.scheduler = None
        else:
            self.scheduler = self.schedulers[scheduler]

        self.loss_fn = (
            nn.BCEWithLogitsLoss() if self.num_classes == 1 else nn.CrossEntropyLoss()
        )

        self.acc = Accuracy(
            task="binary" if self.num_classes == 1 else "multiclass", num_classes=self.num_classes
        )

        self.model = self.resnets[resnet_version](pretrained=transfer)
        linear_input_size = list(self.model.children())[-1].in_features
        self.model.fc = nn.Linear(linear_input_size, self.num_classes)

        blocks_cnt = len([_ for _ in self.model.children()])

        for i, child in enumerate(self.model.children()):
            for param in child.parameters():
                param.requires_grad_ = i >= blocks_cnt - blocks_to_tune

    def on_epoch_start(self):
        torch.cuda.empty_cache()

    def on_train_epoch_start(self) -> None:
        self.train_loss = []
        self.train_acc = []
    
    def on_train_epoch_end(self):
        self.log('train_loss_epoch', sum(self.train_loss) / len(self.train_loss))
        self.log('train_acc_epoch', sum(self.train_acc) / len(self.train_acc))

    def on_validation_epoch_start(self) -> None:
        self.valid_loss = []
        self.valid_acc= []        

    def on_validation_epoch_end(self) -> None:
        if len(self.valid_loss) > 0 and len(self.valid_acc) > 0:
            self.log('valid_loss_epoch', sum(self.valid_loss) / len(self.valid_loss))
            self.log('valid_acc_epoch', sum(self.valid_acc) / len(self.valid_acc))

    def on_test_epoch_start(self) -> None:
        self.test_loss = []
        self.test_acc= []        

    def on_test_epoch_end(self) -> None:
        if len(self.test_loss) > 0 and len(self.test_acc) > 0:
            self.log('test_loss_epoch', sum(self.test_loss) / len(self.test_loss))
            self.log('test_acc_epoch', sum(self.test_acc) / len(self.test_acc))

    def forward(self, input):
        return self.model(input)
    
    def configure_optimizers(self):
        config = {'optimizer': self.optimizer(self.model.parameters(), lr=self.lr),
                  'monitor': 'valid_acc_epoch'}
        if self.scheduler is not None:
            config['lr_sheduler'] = self.scheduler(config['optimizer'], mode='max', 
                                                   factor=.1, patience=2, verbose=True)
        return config
    
    def _step(self, batch):
        inputs, targets = batch
        preds = self.model(inputs)

        if self.num_classes == 1:
            preds = preds.flatten()
            targets = targets.float()

        loss = self.loss_fn(preds, targets)
        acc = self.acc(preds, targets)

        return loss, acc
    
    def _dataloader(self, data_path: str, shuffle: bool = False, more_augmentations: bool = False):
        if more_augmentations:
            transform = transforms.Compose([
                transforms.Resize((244, 244)),
                transforms.RandomHorizontalFlip(0.5),
                transforms.ColorJitter(0.35, 0.3, 0.4, 0.15),
                transforms.RandomSolarize(0.3),
                transforms.ToTensor(),
                transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
            ])
        else:
            transform = transforms.Compose([
                transforms.Resize((244, 244)),
                transforms.ToTensor(),
                transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
            ])

        img_folder = ImageFolder(data_path, transform=transform)

        return DataLoader(img_folder, batch_size=self.batch_size, shuffle=shuffle, num_workers=4)

    def train_dataloader(self):
        return self._dataloader(self.train_path, shuffle=True, more_augmentations=True)

    def training_step(self, batch, batch_idx):
        loss, acc = self._step(batch)

        self.log("train_loss", loss, on_step=True, on_epoch=True)
        self.log("train_acc", acc, on_step=True, on_epoch=True)

        self.train_loss.append(loss)
        self.train_acc.append(acc)

        return loss

    def val_dataloader(self):
        return self._dataloader(self.val_path)

    def validation_step(self, batch, batch_idx):
        loss, acc = self._step(batch)

        self.log("valid_loss", loss, on_step=True, on_epoch=True)
        self.log("valid_acc", acc, on_step=True, on_epoch=True)

        self.valid_loss.append(loss)
        self.valid_acc.append(acc)

        return acc

    def test_dataloader(self):
        return self._dataloader(self.test_path)

    def test_step(self, batch, batch_idx):
        loss, acc = self._step(batch)

        self.log("test_loss", loss, on_step=True, on_epoch=True)
        self.log("test_acc", acc, on_step=True, on_epoch=True)

        return acc

def main():
    DATA_PATH = os.getcwd() + '/data/'
    TRAIN_PATH = DATA_PATH + 'train/'
    VAL_PATH = DATA_PATH + 'val/'
    DEVICE = 'cuda:0' if torch.cuda.is_available() else 'cpu'

    # TEST_PATH = DATA_PATH + 'test/'
    parser = ArgumentParser(prog='ResNetClassifier project')
    parser.add_argument(
        "--model",
        help="Choose one of the predefined ResNet models provided by torchvision.",
        default=18,
        type=int, 
    )
    parser.add_argument(
        "--num_classes", 
        help="Number of classes to be learned.", 
        default=6,
        required=True,
        type=int, 
    )
    parser.add_argument(
        "--num_epochs", 
        help="Number of Epochs to Run.", 
        default=5,
        type=int, 
    )
    parser.add_argument(
        "--train_set", 
        help="Path to training data folder.", 
        default=TRAIN_PATH,
    )
    parser.add_argument(
        "--val_set",
        help="Path to validation set folder.", 
        default=VAL_PATH,
    )
    parser.add_argument(
        "--test_set",
        help="Path to test set folder.", 
        default=None,
        type=str,
    )
    parser.add_argument(
        "--optimizer", '-o',
        help="PyTorch optimizer to use.", 
        default='adam',
    )
    parser.add_argument(
        "--learning_rate", "-lr",
        help="Adjust learning rate of optimizer.",
        default=1e-3,
        type=float, 
    )
    parser.add_argument(
        "--batch_size", "-b",
        help="""Manually determine batch size. Defaults to 16.""",
        default=16,
        type=int, 
    )
    parser.add_argument(
        "--scheduler", "-sch", 
        help="Type of scheduler for learning rate.",
        default=None,
        type=str, 
    )
    parser.add_argument(
        "--transfer", "-tr",
        help="Determine whether to use pretrained model or train from scratch.",
        default=True,
        action="store_true",
    )
    parser.add_argument(
        "--blocks_to_tune",
        help="Determine count of blocks to tune.",
        default=1,
        type=int
    )
    parser.add_argument(
        "-s", "--save_path", 
        help="Path to save model trained model checkpoint.",
    )
    parser.add_argument(
        "-g", "--gpus", 
        help="Enables GPU acceleration.", 
        default=None,
        type=int, 
    )
    parser.add_argument(
        "-g_cnt", "--gpu_count",
        help="Count of gpu devices",
        default=0
    )

    args = parser.parse_args()
    
    pl.seed_everything(0)

    task = Task.init(project_name='DeepSchool Project1', task_name='init')
    tb_logger = pl.loggers.TensorBoardLogger(
        'logs',
        name='1st run'
    )

    model = ResNetClassifier(
        num_classes=int(args.num_classes),
        resnet_version=args.model,
        train_path=args.train_set,
        val_path=args.val_set,
        test_path=args.test_set,
        optimizer=args.optimizer,
        lr=args.learning_rate,
        batch_size=args.batch_size,
        blocks_to_tune=args.blocks_to_tune,
        transfer=args.transfer,
        scheduler='plateau'
    )

    save_path = args.save_path if args.save_path is not None else "./models"
    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        dirpath=save_path,
        filename="resnet-model-{epoch}-{val_loss:.2f}-{valid_acc_epoch:0.2f}",
        monitor="valid_acc_epoch",
        save_top_k=3,
        mode="max",
        save_last=True,
    )

    stopping_callback = pl.callbacks.EarlyStopping(
        monitor='valid_acc_epoch', 
        mode="max", 
        patience=6
    )

    trainer_args = {
        "accelerator": "gpu",
        "devices": [0],
        "max_epochs": args.num_epochs,
        "callbacks": [checkpoint_callback, stopping_callback],
        "precision": 16,
        "detect_anomaly": True, # val_loss стал равен nan, поэтому добавил и перезапустил,
        "logger": tb_logger
    }

    trainer = pl.Trainer(**trainer_args)

    trainer.fit(model)
    torch.save(trainer.model.model.state_dict(), save_path + f"/resnet_{args.model}.pt")


if __name__ == '__main__':
    main()
