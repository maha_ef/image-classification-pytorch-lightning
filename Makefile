PORT?=1234

run_learning:
	 ./venv/bin/python3.10 ./src/model_learning.py --num_classes=6 --num_epochs=25 --batch_size=64 --blocks_to_tune=3 --model=50

install: run_learning # install all
.PHONY: install

.DEFAULT_GOAL := install